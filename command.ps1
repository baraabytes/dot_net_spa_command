# global instalation for SpaTemplates 
dotnet new --install Microsoft.AspNetCore.SpaTemplates::*

# Hint to list available SpaTemplates
dotnet new 

# to create new reactredux SpaTemplate
dotnet new reactredux

# to run the project we must restore .net dependency and npm modules
dotnet restore
npm install 

# start the app by running npm run (then it runs at localhost:5000/ )
dotnet run 